const path = require('path')


exports.createPages = ({ actions, boundActionCreators, graphql }) => {

    const { createPage } = boundActionCreators;

    const postTemplate = path.resolve('./src/templates/post.js')

    return graphql(`{
        allMarkdownRemark {
            edges{
                node{
                    html
                    id
                    frontmatter{
                        path
                        title
                        description
                        date
                    }
                }
            }
        }
    }`).then(res => {

            if (res.errors) {
                return Promise.reject(res.errors)
            }
            let posts = res.data.allMarkdownRemark.edges;

            posts.forEach(({ node }, index) => {

                const previous = index === posts.length - 1 ? null : posts[index + 1].node;
                const next = index === 0 ? null : posts[index - 1].node;


                createPage({
                    path: node.frontmatter.path,
                    component: postTemplate,
                    context: {
                        path: node.frontmatter.path,
                        previous,
                        next
                    }
                })
            });






        })




}