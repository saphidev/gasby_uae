module.exports = {
    siteMetadata: {
        title: "مدونة سيدتي",
        description: "site to ..",
        author: "safi",
        siteUrl: 'http://localhost:3000/',
        social: {
            twitter: "@saphidev"
        }
    },
    plugins: [
        {
            resolve: `gatsby-plugin-typography`,
            options: {
                pathToConfigModule: `src/utils/typography`,
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              path: `${__dirname}/src/articles`,
              name: 'articles',
            },
        },
        `gatsby-transformer-remark`,
    ]
}