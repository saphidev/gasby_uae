import React from 'react'
import { rhythm, scale } from '../utils/typography'

import Ads from './Ads'
export default ({ children }) => <div className="postcontent" style={{
    margin: "0 auto",
    maxWidth: rhythm(24),
    padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`
    }}>
    <Ads />
   

    {children}

    

</div>