import React from 'react'
import { Helmet } from "react-helmet";
import { graphql, StaticQuery } from 'gatsby'
import _ from 'lodash'


const query = graphql`
  query GetSiteMetadata {
    site {
      siteMetadata {
        title
        author
        description
        social {
            twitter
        }
      }
    }
  }
`

class SEO extends React.Component {

    constructor() {
        super();

    }

    render() {

        return <StaticQuery
            query={query}
            render={(data) => {

                let siteMeta = data.site.siteMetadata

                let postDesc = this.props.postDesc || siteMeta.description
                let postAuth = this.props.postAuth || siteMeta.author

                return <Helmet
                    htmlAttributes={{ lang: 'ar',dir:"rtl" }}
                    title={this.props.title || "-"}
                    meta={[
                        {
                            meta: 'description',
                            content: postDesc
                        },
                        {
                            meta: 'og:description',
                            content: postDesc
                        },
                        {
                            name: 'twitter:card',
                            content: 'summary',
                        },
                        {
                            meta: 'author',
                            content: postAuth
                        },
                        {
                            name: 'twitter:creator',
                            content: siteMeta.social.twitter,
                        },
                        {
                            property: 'og:title',
                            content: this.props.title || siteMeta.title,
                        },
                        {
                            name: 'twitter:title',
                            content: this.props.title || siteMeta.title,
                        },
                        {
                            name: 'twitter:description',
                            content: postDesc,
                        },
                    ]}
                />



            }}
        />
    }



};


export default SEO