import React from 'react'

// Import typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

import profilePic from './0.jpeg'
import { rhythm } from '../utils/typography'

class Bio extends React.Component {
  render() {
    return (
      <div
        style={{
          marginTop:'20px',
          display: 'flex',
          marginBottom: rhythm(2.5),
        }}
      >
        <img
          src={profilePic}
          alt={`Saphi`}
          style={{
            marginRight: rhythm(1 / 2),
            marginBottom: 0,
            width: rhythm(2),
            height: rhythm(2),
          }}
        />
        <p style={{ maxWidth: 310,padding:4,fontSize:12 }}>
          مدونة مصممة من  <a href="https://mobile.twitter.com/saphidev">Safi</a>.
          {' '}
          أسئلة و أجوبة مختارة من منتديات عربية
        </p>
      </div>
    )
  }
}

export default Bio