import React from 'react'
import { rhythm, scale } from '../utils/typography'
import Link from 'gatsby-link'
class Header extends React.Component {


    render() {

        let { title, date, siteTitle } = this.props


        title = title || "Untitled"
        return <div style={{ marginTop: 22, padding: 4 }} >

            <h3
                style={{
                    fontFamily: 'Montserrat, sans-serif',
                    marginTop: 0,
                    marginBottom: rhythm(-1),
                }}
            >
                <Link
                    style={{
                        boxShadow: 'none',
                        textDecoration: 'none',
                        color: '#ffa7c4',
                    }}
                    to={'/'}
                >
                    {siteTitle}
                </Link>
            </h3>


            <h1 style={{ marginBottom: 4,padding:8,borderTop:'1px solid #ccc' }}>{title}</h1>
            {date && <p style={{fontSize:12,padding:2}}>تمت الإضافة يوم: {date}</p>}
        </div>
    }

}


export default Header