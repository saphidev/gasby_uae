import React from 'react'

import { rhythm } from '../utils/typography'

class Footer extends React.Component {
  render() {
    return (
      <footer
        style={{
          marginTop: rhythm(2.5),
          paddingTop: rhythm(1),
        }}
      >
        <div style={{ float: 'right' }}>
          <a
            href="/rss.xml"
            target="_blank"
            rel="noopener noreferrer"
          >rss</a>
        </div>  &bull;{' '}
        <a
          href="https://mobile.twitter.com/saphidev"
          target="_blank"
          rel="noopener noreferrer"
        >
          twitter
        </a> &bull;{' '}
        <a
          href="https://github.com/apotox"
          target="_blank"
          rel="noopener noreferrer"
        >
          github
        </a> &bull;{' '}
      </footer>
    )
  }
}

export default Footer