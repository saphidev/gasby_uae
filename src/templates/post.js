import React from 'react'
import Layout from '../components/layout'
import Header from '../components/header'
import SEO from '../components/seo'
import { graphql } from 'gatsby';
import Link from 'gatsby-link'
import _ from 'lodash'
import Bio from '../components/Bio'

class Post extends React.Component {

    constructor() {
        super()
    }


    render() {

        //const  {markdownRemark : post} = this.props.data

        console.log("this.props.data", this.props.data)
        const { markdownRemark: post, site } = this.props.data

        const { previous, next } = this.props.pageContext

        if (!post) return 'not found!'

        let siteUrl = site.siteMetadata.siteUrl

        const postTitle = post ? post.frontmatter.title : site.siteMetadata.title
        const postDesc = post ? post.frontmatter.description : site.siteMetadata.description
        const labels = _.get(post, "frontmatter.labels", "").split(",")



        let seoDate = {
            title: postTitle,
            description: postDesc
        }
        let headerData = {
            siteTitle: site.siteMetadata.title,
            title: postTitle,
            date: post.frontmatter.date,
        }

        return <Layout>
            <SEO {...seoDate} />
            <Header {...headerData} />
            <div >
                <div dangerouslySetInnerHTML={{ __html: post.html }} />
            </div>


            
                        <div className="postlabels">{
                            labels.map(label => <a style={{ margin: 4 }} ref="nofollow" target="_blank" href={`https://www.google.com/search?q=site:${siteUrl}%20inurl:${label}`}>{label}</a>)
                        }
                        </div>
                    

            <ul
                style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    listStyle: 'none',
                    padding: 0,
                }}
            >
                <li>
                    {previous && (
                        <Link to={previous.frontmatter.path} rel="prev">
                            ← {previous.frontmatter.title}
                        </Link>
                    )}
                </li>
                <li>
                    {next && (
                        <Link to={next.frontmatter.path} rel="next">
                            {next.frontmatter.title} →
              </Link>
                    )}
                </li>
            </ul>

            <Bio />
            <br />
        </Layout>


    }





}
export default Post

export const postQuery = graphql`
  query BlogPostBySlug($path: String!) {
    site {
      siteMetadata {
        title
        author
        siteUrl
      }
    }
    markdownRemark( frontmatter: { path: { eq: $path } }) {
      id
      html
      timeToRead
      frontmatter {
        title
        description
        date
        labels
      }
    }
  }
`




