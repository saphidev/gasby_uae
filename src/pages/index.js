import React from "react"
import Link from 'gatsby-link'
import Layout from '../components/layout'
import SEO from '../components/seo'
import Header from '../components/header'
import { graphql, StaticQuery } from 'gatsby'
import Footer from '../components/Footer'
import _ from 'lodash'
import Bio from '../components/Bio'
import { Container, Row, Col } from 'react-grid-system'
import Ads from '../components/Ads'


// export default () => <Layout>
//     <StaticQuery
//         query={graphql`
//     query {
//       file(name :{eq:"page1"}) {
//         id
//         relativePath
//       }
//     }
//   `}

//         render={data => <div>
//             <SEO />
//             <Header />

//             <Footer />

//         </div>}

//     />


// </Layout>

class Main extends React.Component {




    render() {
        const siteTitle = _.get(this, 'props.data.site.siteMetadata.title')
        const siteDescription = _.get(
            this,
            'props.data.site.siteMetadata.description'
        )
        const nodes = _.get(this, 'props.data.allMarkdownRemark.edges')

        console.log("nodes",nodes);
        return <Layout>

            <SEO title={`${siteTitle} | الرئيسية`}/>
            <Header title="الرئيسية"/>

            <Container>
                <Row>
                    <Col sm={6}>
            {
                nodes.map(({node},index)=><div key={`post_${index}`}>
                    <h3 style={{marginBottom:4}}><Link to={node.frontmatter.path} >{node.frontmatter.title}</Link></h3>
                    <small>
                        {node.frontmatter.date}
                        {` •  ☕️ ${node.frontmatter.timeToRead}`}
                    </small>

                </div>)
            }
            </Col >
                    <Col>
                        <Ads />
                    </Col>
                </Row>
            </Container>
            <Bio />
            <br/>
            <Footer />
            
            


        </Layout>
    }


}
export default Main;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }, limit: 20) {
      edges {
        node {
          frontmatter {
            path
            title
            labels
            timeToRead
            date
          }
        }
      }
    }
  }`